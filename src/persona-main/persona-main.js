import {LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-dm/persona-dm.js';

class PersonaMain extends LitElement {
    
    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.showPersonForm = false;
        this.people= [];
        //Fumada
        //this.people = this.shadowRoot.querySelector("persona-dm").people;
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${ this.people.map(
                    person => html`<persona-ficha-listado 
                                        name="${person.name}" 
                                        yearsInCompany="${person.yearsInCompany}"
                                        profile="${person.profile}"
                                        .photo="${person.photo}"
                                        @delete-person="${this.deletePerson}"
                                        @info-person="${this.infoPerson}"
                                    >
                                </persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                <persona-form  id="personForm" class="d-none border rounded border-primary"
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}">
                </persona-form>
            </div>
            <persona-dm
                @people-data-updated="${this.peopleDataUpdated}">
            </persona-dm>
        `;
    }

    updated(changedProperties) {
        console.log("updated");

        //COMENTARIO 19:09h EL PROFE SE QUIERE IR A CASA
        //ERROR GAZAPO GORDO AL LORO!!
        // this.people = this.shadowRoot.querySelector("persona-dm").people;
        
        if(changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if(this.showPersonForm === true){
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if(changedProperties.has("people")) {
            console.log("He cambiado el valor de la propiedad people en persona-main");

            //TO DO Throw event
            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",
                    {
                        detail : {
                            people : this.people
                        }
                    }
                )
            )
        }
    }

    peopleDataUpdated(e) {
        console.log("peopleDataUpdated");

        this.people = e.detail.people;
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);

        this.people = this.people.filter(
                person => person.name != e.detail.name
        );
    }

    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    infoPerson(e) {
        console.log("infoPerson - persona-main");
        console.log("Se ha pedido más información de la persona " + e.detail.name);

        let choosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        console.log(choosenPerson);
        console.log(choosenPerson[0].name);
        
        //ERROR RECURRENTE: EVITAR!!
        //console.log(choosenPerson.name);

        let person = {};
        person.name = choosenPerson[0].name;
        person.profile = choosenPerson[0].profile;
        person.yearsInCompany = choosenPerson[0].yearsInCompany;
        person.photo = choosenPerson[0].photo;

        //this.shadowRoot.querySelector
        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }

    personFormClose() {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");

        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore - inicio");
        console.log(e.detail.person);
        
        if(e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);

            // let indexOfPerson = this.people.findIndex(
            //     person => person.name === e.detail.person.name
            // );

            // if (indexOfPerson >= 0) {
            //     console.log("Persona encontrada");
            //     this.people[indexOfPerson] = e.detail.person;
            // }

            this.people = this.people.map(
                person => person.name === e.detail.person.name  
                    ? person = e.detail.person : person
            );

        } else {
            console.log("Se va a almacenar una persona");    
            
            // ADD
            // this.people.push(e.detail.person);

            // // DELETE
            // this.people = this.people.filter(
            //     person => person.name != e.detail.name
            // );
            
            // // UPDATE
            // this.people[indexOfPerson] = e.detail.person;

            this.people = [...this.people, e.detail.person]; //JS Spread Syntax(Python sequence)
            //this.people = [this.people, e.detail.person]; //Spread syntax: array cuyo 1er elemento es el array original
        }
        
        console.log("personFormStore - fin");
        
        this.showPersonForm = false;
    }

}

customElements.define('persona-main', PersonaMain)