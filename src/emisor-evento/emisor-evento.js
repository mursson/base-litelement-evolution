import { LitElement, html } from 'lit-element';

class EmisorEvento extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h3>Emisor Evento</h3>
            <button @click="${this.sendEvent}">No tocar</button>
        `;
    }

    sendEvent(e) {
        console.log("Dentro de sendEvent");
        console.log(e);

        this.dispatchEvent(
            new CustomEvent(
                "test-event", {
                    "detail" : {
                        "course" : "TechU",
                        "year" : 2021
                    }
                }
            )
        );
        
        this.shadowRoot.getElementById("receiver").classList.showReceptor = true;
    }

}

customElements.define('emisor-evento', EmisorEvento)