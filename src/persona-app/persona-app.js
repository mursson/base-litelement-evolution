import { LitElement, html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

    static get properties() {
        return {
            people : {type: Array}
        };
    }

    constructor() {
        super();

        this.people = [];
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row" style="border-style: dotted;">
                <persona-sidebar class="col-1" @new-person="${this.newPerson}"></persona-sidebar>
                <persona-main class="col-11" @updated-people="${this.updatedPeople}"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
        `;
    }

    updated(changedProperties) {
        console.log(changedProperties);
        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en updated - persona-app");

            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    newPerson(e) {
        console.log("newPerson en persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;

        console.log("showPersonForm " + this.shadowRoot.querySelector("persona-main").showPersonForm);
    }

    updatedPeople(e) {
        console.log("updatedPeople en persona-app");

        this.people = e.detail.people;
    }

    updatedPeopleStats(e) {
        console.log("updatedPeopleStats en persona-app");
        console.log(e.detail);
        console.log(e.detail.peopleStats);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }

}

customElements.define('persona-app', PersonaApp)