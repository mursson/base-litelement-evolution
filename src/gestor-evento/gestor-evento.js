import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement {

    static get properties() {
        return {
            showReceptor: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.showReceptor = false;
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h3>Gestor Evento</h3>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver" class="border rounded border-primary"></receptor-evento>
        `;
    }

    processEvent(e) {
        console.log("Capturado evento del emisor");
        console.log(e);
        console.log("showReceptor: "+this.showReceptor);

        //this.showReceptor = true;
       
        this.shadowRoot.getElementById("receiver").course = e.detail.course;
        this.shadowRoot.getElementById("receiver").year = e.detail.year;
    }

    processEvent2(e) {
        console.log("Capturado evento del emisor");
        console.log(e);
        console.log("showReceptor: "+this.showReceptor);

        this.showReceptor = true;
        //this.shadowRoot.getElementById("receiver").classList.showReceptor = true;
        this.shadowRoot.getElementById("receiver").course = e.detail.course;
        this.shadowRoot.getElementById("receiver").year = e.detail.year;
    }

    updated(changedProperties) {
        console.log("updated");
        if(changedProperties.has("showReceptor")) {
            console.log("Ha cambiado el valor de la propiedad showReceptor en gestor-evento");
            if(this.showReceptor === true){
                this.shadowRoot.getElementById("receiver").classList.add("d-none");
            } else {
                this.shadowRoot.getElementById("receiver").classList.remove("d-none");
            }
        }
    }
}

customElements.define('gestor-evento', GestorEvento)