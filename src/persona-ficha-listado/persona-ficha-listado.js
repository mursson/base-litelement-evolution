import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            profile: {type: String},
            photo: {type: Object}
        };
    }

    // static get styles() {
    //     return css`
    //         div { color: green }
    //         .card-title { 
    //             font-weight: bold;
    //             color: red
    //         }
    //         .list-group-item { 
    //             font-weight: bold;
    //             color: blue
    //         }
    //     `
    // }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div class="card h-100">
                <img src="${this.photo.src}" alt="${this.photo.alt}" height="200" width="50" class="card-img-top"/>
                <div class="card-body">
                    <h5 class="card-title">${this.name}</h5>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>X</strong></button>
                    <button @click="${this.moreInfo}" class="btn btn-info col-5"><strong>+ Info</strong></button>
                </div>
            </div>
        `;
    }

    deletePerson(e) {
        console.log("deletePerson en persona-ficha-listado");
        console.log("Se va a borrar la persona de nombre " + this.name);

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                detail: {
                    name: this.name
                }
            })
        );
    }

    moreInfo(e) {
        console.log("moreInfo en persona-ficha-listado");
        console.log("Se pide mas info de la persona " + this.name);

        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    name: this.name
                }
            })
        );
    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado)