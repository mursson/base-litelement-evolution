import { LitElement, html} from "lit-element";
class PersonaForm extends LitElement {
    
    static get properties() {
        return {
            person: {type: Object},
            editingPerson: { type: Boolean }
        };
    }

    constructor() {
        console.log("Entro en el constructor");

        super();

        this.person = {};

        this.resetFormData();
        
    }

    render() {
        return html`
            <!-- Enlace Bootstrap -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" 
                            @input="${this.updateName}" 
                            id="personFormName" 
                            class="form-control" 
                            placeholder="Nombre Completo" 
                            .value="${this.person.name}"
                            ?disabled="${this.editingPerson}"
                            />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea 
                            @input="${this.updateProfile}" 
                            class="form-control" 
                            placeholder="Perfil" 
                            rows="5"
                            .value="${this.person.profile}"
                            ?disabled="${this.editingPerson}"
                            ></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text"
                             @input="${this.updateYearsInCompany}" 
                             class="form-control" 
                             placeholder="Años en la empresa" 
                             .value="${this.person.yearsInCompany}"
                             ?disabled="${this.editingPerson}"
                             />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();

        this.resetFormData();
        this.dispatchEvent( new CustomEvent("persona-form-close",{}));
    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor "+ e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile con el valor "+ e.target.value);
        this.person.profile = e.target.value;
    }
    
    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad yearsInCompany con el valor "+ e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    resetFormData(e) {
        console.log("resetFormData");
        console.log("Reseteando los input");

        this.person = {};
        this.person.name = "";
        this.person.photo = "";
        this.person.yearsInCompany = "";
        this.person.profile = "";

        this.editingPerson = false;
    }

    storePerson(e) {
        console.log("storePerson");
        e.preventDefault();

        this.person.photo = {
            "src": "./img/persona.jpg",
            "alt": "Persona"
        }

        console.log("La propiedad name vale " + this.person.name);
        console.log("La propiedad profile vale " + this.person.profile);
        console.log("La propiedad yearsInCompany vale " + this.person.yearsInCompany);

        this.dispatchEvent( new CustomEvent("persona-form-store", {
            detail: {
                person : {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson :  this.editingPerson
            }
        }));

        //this.resetFormData();
    }
}


customElements.define('persona-form', PersonaForm)