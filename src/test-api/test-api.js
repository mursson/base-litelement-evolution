import { LitElement, html } from 'lit-element';

class TestAPI extends LitElement {

    static get properties() {
        return {
            movies: { type: Array }
        };
    }

    constructor() {
        super();

        this.movies = [];

        this.getMovieData();

        // this.movies = [
        //     {
        //         title = "Star Wars 1",
        //         director = "John Lucas",
        //     },
        // ];
    }

    render() {
        return html`
            <h1>Test API</h1>

            ${this.movies.map(
                movie => html`<div>La pelicula ${movie.title} fue dirigida por ${movie.director}</div>`
            )}
        `;
    }

    getMovieData() {
        console.log("Dentro del getMovieData()");
        console.log("Obteniendo datos de peliculas");

        //DOCUMENTACIÓN Interesante: https://developer.mozilla.org/es/docs/Web/API/XMLHttpRequest
        let xhr = new XMLHttpRequest();
        
        //Ejemplo de async (se puede hacer después!)
        //Puñetas => console.log(this)
        //Aquí podemos programar comportamientos en función de la respuesta que recibamos
        xhr.onload = () => {
            if (xhr.status === 200){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 200 - OK - Petición realizada correctamente");

                //console.log(xhr.responseText);
                console.log(JSON.parse(xhr.responseText));

                let APIResponse = JSON.parse(xhr.responseText);

                //No podemos asumir que la respuesta de un API sea siempre con este esquema de datos en results
                this.movies = APIResponse.results;

                console.log(APIResponse.results);

            }else if (xhr.status === 400){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

            }else if (xhr.status === 404){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
            }
        }

        xhr.open("GET","https://swapi.dev/api/films/");
        xhr.send();

        console.log("Fin del getMovieData()");
    }

    // Don't use connectedCallback() since it can't be async
    // async firstUpdated() {
    // await fetch(`https://swapi.dev/api/films/`)
    //     .then(r => r.json())
    //     .then(async data => {
    //     this.movies = data.result;
    //     });
    // }
}

customElements.define('test-api', TestAPI)