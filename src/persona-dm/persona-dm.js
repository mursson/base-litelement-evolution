import { LitElement, html } from 'lit-element';

class PersonaDM extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                photo: {
                    "src": "./img/EllenRipley.jpg",
                    "alt": "Ellen Ripley"
                }, 
                profile: "Lorem ipsum dolor sit amet."
            }, {
                name: "Bruce Banner",
                yearsInCompany: 2,
                photo: {
                    "src": "./img/BruceBanner.jpg",
                    "alt": "Bruce Banner"
                }, 
                profile: "Lorem ipsum."
            }, {
                name: "Éowyn",
                yearsInCompany: 5,
                photo: {
                    "src": "./img/eowyn.png",
                    "alt": "Éowyn"
                }, 
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
            }, {
                name: "Turanga Leela",
                yearsInCompany: 9,
                photo: {
                    "src": "./img/TurangaLeela.jpg",
                    "alt": "Turanga Leela"
                }, 
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit."
            }, {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                photo: {
                    "src": "./img/tyrion.jpg",
                    "alt": "Tyrion Lannister"
                }, 
                profile: "Lorem ipsum."
            }
        ];

    }

    updated(changedProperties) {
        console.log("updated");

        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la prpiedad people");

            this.dispatchEvent(
                new CustomEvent(
                    "people-data-updated",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            )
        }
    }

    // render() {
    //     return html`
    //     <h1>Persona DM</h1>
    //     `;
    // }
}

customElements.define('persona-dm', PersonaDM)